"""
Created on Tue Sep 15 17:11:39 2015
@author: Max W. Y. Lam

Prerequisite: python2.7/python3.4 & scikit-learn >=0.18
"""
import sys
sys.path.append("./*")
import numpy as np
import gpr
import kernels
from sklearn.metrics import mean_squared_error
from sklearn.externals.six.moves import xrange
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.ensemble.gradient_boosting import GradientBoostingRegressor
from sklearn.ensemble.forest import RandomForestRegressor


# Load 1d function data
def load_1d_function_data(proportion=0.9):

    def test_func(X):
        return np.sin(X)

    N = 500
    noise = 0.1
    X = np.vstack((np.random.rand(N/2, 1)*N/50,
                   15+np.random.rand(N/2, 1)*N/50))
    y = test_func(X) + np.random.randn(X.shape[0], 1) * noise
    X = X.astype(np.float32)
    offset = int(X.shape[0] * proportion)
    X_train, y_train = X[:offset], y[:offset]
    X_test, y_test = X[offset:], y[offset:]
    return X_train, y_train, X_test, y_test

# Load boston data
def load_boston_data(proportion=0.8):
    from sklearn import datasets
    from sklearn.utils import shuffle
    boston = datasets.load_boston()
    X, y = shuffle(boston.data, boston.target,
                   random_state=np.random.RandomState(np.random.randint(1001)))
    y = y[:, None]
    X = X.astype(np.float32)
    offset = int(X.shape[0] * proportion)
    X_train, y_train = X[:offset], y[:offset]
    X_test, y_test = X[offset:], y[offset:]
    return X_train, y_train, X_test, y_test

if(__name__ == "__main__"):
    import socket
    result_log = open('log_'+socket.gethostname().split('.')[0]+'_2.txt', 'wt')
    threds = [50, 80, 100]
    layers = [2, 1, 0]
    cen_algs = ["median"]
    cmp_algs = ["norm1", "norm2", "canberra"]
    for _ in xrange(3):
        X_train, y_train, X_test, y_test = load_boston_data()
        # Gradient Boosting Regressor
        model_gbr = GradientBoostingRegressor(
            alpha=0.9, init=None, learning_rate=0.1, loss='ls',
            max_depth=3, max_features=None, max_leaf_nodes=None,
            min_samples_leaf=1, min_samples_split=2,
            min_weight_fraction_leaf=0.0, n_estimators=300,
            random_state=None, subsample=1.0, verbose=0, warm_start=False)
        model_gbr.fit(X_train, y_train.ravel())
        y_pred = model_gbr.predict(X_test)
        mse_gbr = mean_squared_error(y_test.ravel(), y_pred)
        result_log.write("\n \t GBR MSE = %.5f\n" % (mse_gbr))
        result_log.flush()
        # Random Forest Regressor
        model_rfr = RandomForestRegressor(
            bootstrap=True, criterion='mse', max_depth=None,
            max_features='auto', max_leaf_nodes=None, min_samples_leaf=1,
            min_samples_split=2, min_weight_fraction_leaf=0.0,
            n_estimators=300, n_jobs=1, oob_score=False, random_state=None,
            verbose=0, warm_start=False)
        model_rfr.fit(X_train, y_train.ravel())
        y_pred = model_rfr.predict(X_test)
        mse_rfr = mean_squared_error(y_test.ravel(), y_pred)
        result_log.write("\n \t RFR MSE = %.5f\n" % (mse_rfr))
        result_log.flush()
        # Ada Boost Regressor
        model_ada = AdaBoostRegressor(
            DecisionTreeRegressor(max_depth=5),
            n_estimators=300, random_state=np.random.RandomState(1))
        model_ada.fit(X_train, y_train.ravel())
        y_pred = model_ada.predict(X_test)
        mse_ada = mean_squared_error(y_test.ravel(), y_pred)
        result_log.write("\n \t ADA MSE = %.5f\n" % (mse_ada))
        result_log.flush()
        # Clustered 2-layer Sparse Gaussian Process Regression
        for thred in threds:
            for cen_alg in cen_algs:
                for cmp_alg in cmp_algs:
                    for layer in layers:
                        model_cgpr = gpr.cgpr(kernels.rq(), thred, layer, cen_alg, cmp_alg)
                        model_cgpr.fit(X_train, y_train)
                        if(X_test.shape[1] == 1):
                            model_cgpr.plot_centers()
                            model_cgpr.plot()
                        y_pred = model_cgpr.predict(X_test)
                        mse_cgpr = mean_squared_error(y_test, y_pred)
                        result_log.write("\n \t CGPR %s %d %d %s %s MSE = %.5f\n" %
                                        (model_cgpr.mu_kern.name(), thred, layer, cen_alg, cmp_alg, mse_cgpr))
                        result_log.flush()
