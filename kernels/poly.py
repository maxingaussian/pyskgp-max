"""
Created on Sun Oct 18 00:45:13 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .kernel import kernel

""""""""""""""" Polynomial Kernel """""""""""""""


class poly(kernel):

    # hyperparameters:
    # bias(bias), coefficient(coef), degree(deg)
    bias, coef, deg = 0, [], 0

    def __init__(self, dim=1, ard=False):
        self.dim = dim
        if(not ard):
            self.dim = 1
        self.n = 4 + 1 * self.dim
        super(poly, self).__init__(self.dim, ard)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        self.bias = params[2]
        self.coef = params[3:3+self.dim]
        self.deg = np.ceil(2 * params[3+self.dim])
        super(poly, self).set_params(params)

    def name(self):
        return "Polynomial" + ("ARD" if self.ard else "ISO")

    def k(self, x1, x2):
        r = self.bias
        if(self.dim == 1):
            r += self.coef[0] * x1 * x2
            return self.sigma ** 2 * r.sum() ** (1 + self.deg)
        else:
            r += self.coef * x1 * x2
            return self.sigma ** 2 * r.sum() ** (1 + self.deg)

    def dk_dp(self, x1, x2, d):
        r = self.bias
        if(self.dim == 1):
            r += self.coef[0] * x1 * x2
        else:
            r += self.coef * x1 * x2
        if(self.dim == 1):
            if(d == 1):
                return 2 * self.sigma * r.sum() ** (1 + self.deg)
            elif(d == 2):
                return self.sigma ** 2 * (self.deg + 1) * r.sum() **\
                    (self.deg)
            elif(d == 3):
                return self.sigma ** 2 * (self.deg + 1) * x1[0] * x2[0] *\
                    r.sum() ** (self.deg)
            elif(d == 4):
                return 2 * self.k(x1, x2) * np.log(r.sum())
        else:
            if(d == 1):
                return 2 * self.sigma * r.sum() ** (1 + self.deg)
            elif(d == 2):
                return self.sigma ** 2 * (self.deg + 1) * r.sum() ** (self.deg)
            elif(3+self.dim > d >= 3):
                return self.sigma ** 2 * (self.deg + 1) * x1[d-3] * x2[d-3] *\
                    r.sum() ** (self.deg)
            elif(d == 3+self.dim):
                return 2 * self.k(x1, x2) * np.log(r.sum())
