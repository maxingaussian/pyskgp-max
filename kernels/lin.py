"""
Created on Sun Oct 18 00:44:30 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .kernel import kernel

""""""""""""""" Linear Kernel """""""""""""""


class lin(kernel):

    # hyperparameters:
    # coefficient(coef), bias(bias)
    bias, coef = 0, []

    def __init__(self, dim=1, ard=False):
        self.dim = dim
        if(not ard):
            self.dim = 1
        self.n = 3 + 1 * self.dim
        super(lin, self).__init__(self.dim, ard)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        self.bias = params[2]
        self.coef = params[3:3+self.dim]
        super(lin, self).set_params(params)

    def name(self):
        return "Linear" + ("ARD" if self.ard else "ISO")

    def k(self, x1, x2):
        r = self.bias
        if(self.dim == 1):
            r += self.coef[0] * x1 * x2
        else:
            r += self.coef * x1 * x2
        return self.sigma ** 2 * r.sum()

    def dk_dp(self, x1, x2, d):
        r = self.bias
        if(self.dim == 1):
            r += self.coef[0] * x1 * x2
        else:
            r += self.coef * x1 * x2
        if(d == 1):
            return 2 * self.sigma * r.sum()
        elif(d == 2):
            return self.sigma ** 2
        elif(3+self.dim > d >= 3):
            return self.sigma ** 2 * x1[d-3] * x2[d-3]
