"""
Created on Sun Oct 18 00:47:32 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .kernel import kernel

""""""""""""""" L1 Spectral Mixture Kernel """""""""""""""


class l1sm(kernel):

    gmnum = 1
    gmw, mu, cov, ls = [], [], [], []

    def __init__(self, dim, gmnum, ard):
        self.dim = dim
        self.gmnum = gmnum
        if(not ard):
            self.dim = 1
        self.n = 2 + 3 * self.gmnum + self.dim
        super(l1sm, self).__init__(self.dim, ard)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        super(l1sm, self).set_params(params)
        self.gmw = params[2:2+self.gmnum]
        self.mu = params[2+self.gmnum:2+2*self.gmnum]
        self.cov = params[2+2*self.gmnum:2+3*self.gmnum]
        self.ls = params[2+3*self.gmnum:self.n]

    def name(self):
        return "Spectral Mixture" + "ARD" if self.ard else "ISO"

    def k(self, x1, x2):
        r = (x1 - x2)
        res = 0
        for q in xrange(self.gmnum):
            cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
            exp = np.prod(np.exp(-2*np.abs(r)*np.square(np.pi * self.cov[q] /
                          (self.ls[0] if self.dim == 1 else self.ls))))
            res += (self.gmw[q] ** 2) * cos * exp
        return (self.sigma ** 2) * res

    def dk_dp(self, x1, x2, d):
        if(d == 1):
            return 2 * self.k(x1, x2) / self.sigma
        r = (x1 - x2)
        if(2 <= d < 2+self.gmnum):
            q = d-2
            cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
            exp = np.prod(np.exp(-2*np.abs(r)*np.square(np.pi * self.cov[q] /
                          (self.ls[0] if self.dim == 1 else self.ls))))
            return (self.sigma ** 2) * 2 * self.gmw[q] * cos * exp
        if(2+self.gmnum <= d < 2+2*self.gmnum):
            q = d-2-self.gmnum
            cos = np.prod(-2*np.pi*r*np.sin(2 * np.pi * r * self.mu[q]))
            exp = np.prod(np.exp(-2 * np.square(np.pi * self.cov[q] * r /
                          (self.ls[0] if self.dim == 1 else self.ls))))
            return (self.sigma ** 2) * (self.gmw[q] ** 2) * cos * exp
        if(2+2*self.gmnum <= d < 2+3*self.gmnum):
            q = d-2-2*self.gmnum
            cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
            cof = (np.abs(r) * np.square(np.pi /
                   (self.ls[0] if self.dim == 1 else self.ls)))
            exp = np.prod(-4 * self.cov[q] * cof *
                          np.exp(-2 * cof * (self.cov[q] ** 2)))
            return (self.sigma ** 2) * (self.gmw[q] ** 2) * cos * exp
        if(2+3*self.gmnum <= d < self.n):
            di = d-2-3*self.gmnum
            res = 0
            for q in xrange(self.gmnum):
                cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
                exp = np.prod(np.exp(-2*np.abs(r)*np.square(np.pi*self.cov[q] /
                              (self.ls[0] if self.dim == 1 else self.ls))))
                exp *= 4 * np.abs(r[di])*np.square(np.pi * self.cov[q]) /\
                    ((self.ls[0] if self.dim == 1 else self.ls) ** 3)
                res += (self.gmw[q] ** 2) * cos * exp
            return (self.sigma ** 2) * res

""""""""""""""" L2 Spectral Mixture Kernel """""""""""""""


class l2sm(kernel):

    # http://jmlr.org/proceedings/papers/v28/wilson13.pdf

    gmnum = 1
    gmw, mu, cov, ls = [], [], [], []

    def __init__(self, dim, gmnum, ard):
        self.dim = dim
        self.gmnum = gmnum
        if(not ard):
            self.dim = 1
        self.n = 2 + 3 * self.gmnum + self.dim
        super(l2sm, self).__init__(self.dim, ard)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        super(l2sm, self).set_params(params)
        self.gmw = params[2:2+self.gmnum]
        self.mu = params[2+self.gmnum:2+2*self.gmnum]
        self.cov = params[2+2*self.gmnum:2+3*self.gmnum]
        self.ls = params[2+3*self.gmnum:self.n]

    def name(self):
        return "Spectral Mixture" + "ARD" if self.ard else "ISO"

    def k(self, x1, x2):
        r = (x1 - x2)
        res = 0
        for q in xrange(self.gmnum):
            cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
            exp = np.prod(np.exp(-2 * np.square(np.pi * self.cov[q] * r /
                          (self.ls[0] if self.dim == 1 else self.ls))))
            res += (self.gmw[q] ** 2) * cos * exp
        return (self.sigma ** 2) * res

    def dk_dp(self, x1, x2, d):
        if(d == 1):
            return 2 * self.k(x1, x2) / self.sigma
        r = (x1 - x2)
        if(2 <= d < 2+self.gmnum):
            q = d-2
            cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
            exp = np.prod(np.exp(-2 * np.square(np.pi * self.cov[q] * r /
                          (self.ls[0] if self.dim == 1 else self.ls))))
            return (self.sigma ** 2) * 2 * self.gmw[q] * cos * exp
        if(2+self.gmnum <= d < 2+2*self.gmnum):
            q = d-2-self.gmnum
            cos = np.prod(-2*np.pi*r*np.sin(2 * np.pi * r * self.mu[q]))
            exp = np.prod(np.exp(-2 * np.square(np.pi * self.cov[q] * r /
                          (self.ls[0] if self.dim == 1 else self.ls))))
            return (self.sigma ** 2) * (self.gmw[q] ** 2) * cos * exp
        if(2+2*self.gmnum <= d < 2+3*self.gmnum):
            q = d-2-2*self.gmnum
            cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
            cof = np.square(np.pi * r /
                            (self.ls[0] if self.dim == 1 else self.ls))
            exp = np.prod(-4 * self.cov[q] * cof *
                          np.exp(-2 * cof * (self.cov[q] ** 2)))
            return (self.sigma ** 2) * (self.gmw[q] ** 2) * cos * exp
        if(2+3*self.gmnum <= d < self.n):
            di = d-2-3*self.gmnum
            res = 0
            for q in xrange(self.gmnum):
                cos = np.prod(np.cos(2 * np.pi * r * self.mu[q]))
                exp = np.prod(np.exp(-2 * np.square(np.pi * self.cov[q] * r /
                              (self.ls[0] if self.dim == 1 else self.ls))))
                exp *= 4 * np.square(np.pi * self.cov[q] * r[di]) /\
                    ((self.ls[0] if self.dim == 1 else self.ls) ** 3)
                res += (self.gmw[q] ** 2) * cos * exp
            return (self.sigma ** 2) * res
