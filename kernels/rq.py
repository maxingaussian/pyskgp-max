"""
Created on Sun Oct 18 00:46:26 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .kernel import kernel

""""""""""""""" Rational Quadratic Kernel """""""""""""""


class rq(kernel):

    # hyperparameters:
    # lengthscale(ls), relative weight(rw)
    ls, rw = [], []

    def __init__(self, dim=1, ard=False):
        self.dim = dim
        if(not ard):
            self.dim = 1
        self.n = 2 + 2 * self.dim
        super(rq, self).__init__(self.dim, ard)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        super(rq, self).set_params(params)
        self.ls = params[2:2+self.dim]
        self.rw = params[2+self.dim:2+2*self.dim]

    def name(self):
        return "RationalQuadratic" + ("ARD" if self.ard else "ISO")

    def k(self, x1, x2):
        r = (x1 - x2)
        if(self.dim == 1):
            r = np.square(r / self.ls[0]) / self.rw[0]
        else:
            r = np.square(r / self.ls) / self.rw
        if(self.dim == 1):
            return self.sigma ** 2 * ((1 + 0.5 * r) **
                                      (-1 * self.rw[0])).sum()
        else:
            return self.sigma ** 2 * ((1 + 0.5 * r) **
                                      (-1 * self.rw)).sum()

    def dk_dp(self, x1, x2, d):
        r = (x1 - x2)
        if(self.dim == 1):
            r = np.square(r / self.ls[0]) / self.rw[0]
        else:
            r = np.square(r / self.ls) / self.rw
        if(self.dim == 1):
            if(d == 1):
                return 2 * self.sigma * ((1 + 0.5 * r) **
                                         (-1 * self.rw[0])).sum()
            elif(2+self.dim > d >= 2):
                return self.sigma ** 2 * ((1 + 0.5 * r) **
                                          (-1 * self.rw[0] - 1)).sum()\
                    * r[d - 2] * self.rw[d - 2] / self.ls[d - 2]
            elif(d >= 2+self.dim):
                logt = np.log(0.5 * r + 1)
                coef = 0.5 * r / (0.5 * r + 1) - logt
                return self.sigma ** 2 * ((((1 + 0.5 * r) **
                                            (-1 * self.rw[0]))) * coef).sum()
        else:
            if(d == 1):
                return 2 * self.sigma * ((1 + 0.5 * r) **
                                         (-1 * self.rw)).sum()
            elif(2+self.dim > d >= 2):
                return self.sigma ** 2 * ((1 + 0.5 * r) **
                                          (-1 * self.rw - 1)).sum()\
                    * r[d - 2] * self.rw[d - 2] / self.ls[d - 2]
            elif(d >= 2+self.dim):
                r_d = r[d - (2+self.dim)]
                logt = np.log(0.5 * r_d + 1)
                coef = 0.5 * r_d / (0.5 * r_d + 1) - logt
                return self.sigma ** 2 * ((((1 + 0.5 * r) **
                                            (-1 * self.rw))) * coef).sum()
