"""
Created on Sun Oct 18 00:41:28 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .kernel import kernel

""""""""""""""" Exponential Kernel """""""""""""""


class exp(kernel):

    # hyperparameters:
    # lengthscale(ls)
    ls = []

    def __init__(self, dim=1, ard=False):
        self.dim = dim
        if(not ard):
            self.dim = 1
        self.n = 2 + self.dim
        super(exp, self).__init__(self.dim, ard)

    def name(self):
        return "Exponential" + ("ARD" if self.ard else "ISO")

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        super(exp, self).set_params(params)
        self.ls = params[2:2+self.dim]

    def k(self, x1, x2):
        r = np.abs(x1 - x2)
        if(self.dim == 1):
            r /= np.square(self.ls[0])
        else:
            r /= np.square(self.ls)
        return self.sigma ** 2 * np.exp(-0.5 * r.sum())

    def dk_dp(self, x1, x2, d):
        r = np.abs(x1 - x2)
        if(self.dim == 1):
            r /= np.square(self.ls[0])
        else:
            r /= np.square(self.ls)
        if(d == 1):
            return 2 * self.sigma * np.exp(-0.5 * r.sum())
        elif(2+self.dim > d >= 2):
            return self.sigma ** 2 * np.exp(-0.5 * r.sum())\
                * r[d - 2] / self.ls[d - 2]
