"""
Created on Sun Oct 18 00:45:50 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .kernel import kernel

""""""""""""""" Periodic Kernel """""""""""""""


class per(kernel):

    # hyperparameters:
    # lengthscale(ls), period length(prd)
    ls, prd = [], []

    def __init__(self, dim=1, ard=False):
        self.dim = dim
        if(not ard):
            self.dim = 1
        self.n = 2 + 2 * self.dim
        super(per, self).__init__(self.dim, ard)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        super(per, self).set_params(params)
        self.ls = params[2:2+self.dim]
        self.prd = params[2+self.dim:2+2*self.dim]

    def name(self):
        return "Periodic" + ("ARD" if self.ard else "ISO")

    def k(self, x1, x2):
        r = np.abs(x1 - x2)
        if(self.dim == 1):
            r = np.pi * r / self.prd[0]
            r = np.sin(r) / self.ls[0]
        else:
            r = np.pi * r / self.prd
            r = np.sin(r) / self.ls
        r = np.square(r)
        return self.sigma ** 2 * np.exp(-2 * r.sum())

    def dk_dp(self, x1, x2, d):
        r = np.abs(x1 - x2)
        if(self.dim == 1):
            r = np.pi * r / self.prd[0]
            r = np.sin(r) / self.ls[0]
        else:
            r = np.pi * r / self.prd
            r = np.sin(r) / self.ls
        r = np.square(r)
        if(d == 1):
            return 2 * self.sigma * np.exp(-2 * r.sum())
        elif(2+self.dim > d >= 2):
            return 4 * self.sigma ** 2 * np.exp(-2 * r.sum())\
                * r[d - 2] / self.ls[d - 2]
        elif(d >= 2+self.dim):
            dif = abs(x1[d - (2+self.dim)] - x2[d - (2+self.dim)])
            sin2 = np.sin(2 * np.pi * dif / self.prd[d - (2+self.dim)])
            return 2 * np.pi * self.sigma ** 2 * dif * sin2\
                * np.exp(-2 * r.sum()) / self.ls[d - (2+self.dim)] ** 2\
                / self.prd[d - (2+self.dim)] ** 2
