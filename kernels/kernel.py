"""
Created on Tue Sep 15 13:54:48 2015
@author: Max W. Y. Lam
"""

import abc
import numpy as np
from sklearn.utils import extmath
from sklearn.externals.six.moves import xrange


class kernel(object):

    # *core hyperparameters:
    # noise(noise), sigma(sigma)
    noise, sigma = 0, 0
    setted = False
    fix = [False] * 2

    def __init__(self, dim=1, ard=False):
        self.dim = dim
        self.ard = ard
        from sklearn.utils import check_random_state
        from datetime import datetime
        year = np.random.random_integers(1970, 2015)
        month = np.random.random_integers(1, 12)
        day = np.random.random_integers(1, 28)
        self.seed = int((datetime.now() -
                        datetime(year, month, day)).total_seconds()) % 2 ** 30
        self.rand = check_random_state(self.seed)
        self.randomize_params()

    def __add__(self, other):
        if(isinstance(other, kernel)):
            return plus(self.dim, self, other)
        else:
            return self

    def __radd__(self, other):
        return self.__add__(other)

    def __mul__(self, other):
        if(isinstance(other, kernel)):
            return prod(self.dim, self, other)
        else:
            self.sigma *= np.sqrt(other)
            return self

    def __rmul__(self, other):
        return self.__mul__(other)

    def randomize_params(self):
        self.set_params(np.abs(self.rand.rand(self.n)))

    def K(self, X, noisy=True):
        n = X.shape[0]
        K = np.array([[0 for j in xrange(n)] for i in xrange(n)], np.float_)
        for i in xrange(n):
            K[i, i] = self.k(X[i, :], X[i, :]) + 0.01/n
            if(noisy):
                K[i, i] += self.noise
            for j in xrange(i + 1, n):
                K_ij = self.k(X[i, :], X[j, :])
                K[i, j] = K_ij
                K[j, i] = K_ij
                del K_ij
        return K

    def Ks(self, Xs, X):
        m, n = Xs.shape[0], X.shape[0]
        Ks = np.array([[0 for j in xrange(n)] for i in xrange(m)], np.float_)
        for i in xrange(m):
            for j in xrange(n):
                Ks[i, j] = self.k(Xs[i, :], X[j, :])
        return Ks

    def dK_dP(self, X, d):
        n = X.shape[0]
        if(d == 0):
            return np.eye(n)
        dK = np.array([[0 for j in xrange(n)] for i in xrange(n)], np.float_)
        for i in xrange(n):
            dK[i, i] = self.noise + self.dk_dp(X[i, :], X[i, :], d)
            for j in xrange(i + 1, n):
                dK_ij = self.dk_dp(X[i, :], X[j, :], d)
                dK[i, j] = dK_ij
                dK[j, i] = dK_ij
                del dK_ij
        return dK

    def dKs_dP(self, Xs, X, d):
        m, n = Xs.shape[0], X.shape[0]
        dK = np.array([[0 for j in xrange(n)] for i in xrange(m)], np.float_)
        if(d == 0):
            for i in xrange(min(n, m)):
                dK[i, i] = 1
            return dK
        for i in xrange(m):
            for j in xrange(n):
                dK[i, j] = self.dk_dp(Xs[i, :], X[j, :], d)
        return dK

    def unfix(self):
        self.fix = [False] * 2

    def fix_noise(self, noise=-1):
        if(noise != -1):
            self.noise = max(noise, 1e-5)
        self.fix[0] = True

    def fix_sigma(self, sigma=-1):
        if(sigma != -1):
            self.sig = sigma
        self.fix[1] = True

    def plot_density(self):
        from matplotlib import pyplot as plt
        pts = 500
        X = np.linspace(-10, 10, pts)
        ds = np.asarray([self.k(X[r], 0) for r in xrange(pts)])
        rng = ds.max() - ds.min()
        plt.figure()
        plt.ylim(ds.min() - 0.2*rng, ds.max() + 0.4*rng)
        plt.plot((0, 0), (ds.min()-0.2*rng, ds.max()+0.4*rng), 'k--')
        plt.plot((X.min(), X.max()), (0, 0), 'k--')
        plt.plot(X, ds, linewidth=2.0)
        plt.show()

    def plot_samples(self, num):
        from matplotlib import pyplot as plt
        pts = 500
        X = np.linspace(0, 10, pts)
        K = self.K(X[:, None])
        L = extmath.linalg.cholesky(K, lower=True)
        plt.figure()
        for i in xrange(num):
            plt.plot(X, L.dot(np.random.randn(pts)))
        plt.show()

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def set_params(self, params):
        self.setted = True
        if(isinstance(params, list)):
            params = np.asarray(params)
        if(not self.fix[0]):
            self.noise = max(params[0], 1e-5)
        else:
            params[0] = self.noise
        if(not self.fix[1]):
            self.sigma = params[1]
        else:
            params[1] = self.sigma
        self._params = params

    @abc.abstractmethod
    def name(self):
        pass

    @abc.abstractmethod
    def k(self, x1, x2):
        pass

    @abc.abstractmethod
    def dk_dp(self, x1, x2, d):
        pass


""""""""""""""" Combine Kernels by Addition """""""""""""""


class plus(kernel):

    kern1, kern2 = None, None

    def __init__(self, dim, kern1, kern2):
        self.kern1 = kern1
        self.kern2 = kern2
        self.n = kern1.n + kern2.n - 1
        super(plus, self).__init__(dim, False)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        if(not self.fix[0]):
            self.noise = max(params[0], 1e-5)
        else:
            params[0] = self.noise
        if(not self.fix[1]):
            self.sigma = params[1]
        else:
            params[1] = self.sigma
        self._params = params
        params1 = params[:self.kern1.n]
        self.kern1.set_params(params1)
        params2 = [self.noise] + params[self.kern1.n:self.n].tolist()
        self.kern2.set_params(np.asarray(params2))

    def name(self):
        return "(%s + %s)" % (self.kern1.name(), self.kern2.name())

    def k(self, x1, x2):
        return self.kern1.k(x1, x2) + self.kern2.k(x1, x2)

    def dk_dp(self, x1, x2, d):
        if(d < self.kern1.n):
            return self.kern1.dk_dp(x1, x2, d)
        elif(self.kern1.n <= d < self.n):
            return self.kern2.dk_dp(x1, x2, d - self.kern1.n + 1)


""""""""""""""" Combine Kernels by Multiplication """""""""""""""


class prod(kernel):

    kern1, kern2 = None, None

    def __init__(self, dim, kern1, kern2):
        self.kern1 = kern1
        self.kern2 = kern2
        self.n = kern1.n + kern2.n - 2
        super(prod, self).__init__(dim, False)

    def set_params(self, params):
        if(isinstance(params, list)):
            params = np.asarray(params)
        if(not self.fix[0]):
            self.noise = max(params[0], 1e-5)
        else:
            params[0] = self.noise
        if(not self.fix[1]):
            self.sigma = params[1]
        else:
            params[1] = self.sigma
        self._params = params
        self.sigma = params[1]
        params1 = params[:self.kern1.n]
        self.kern1.set_params(params1)
        params2 = [self.noise, 1.0] +\
            params[self.kern1.n:self.n].tolist()
        self.kern2.set_params(np.asarray(params2))

    def name(self):
        return "(%s * %s)" % (self.kern1.name(), self.kern2.name())

    def k(self, x1, x2):
        return self.kern1.k(x1, x2) * self.kern2.k(x1, x2)

    def dk_dp(self, x1, x2, d):
        if(d < self.kern1.n):
            return self.kern1.dk_dp(x1, x2, d) *\
                self.kern2.k(x1, x2)
        elif(self.kern1.n <= d < self.n):
            return self.kern2.dk_dp(x1, x2, d - self.kern1.n + 2) *\
                self.kern1.k(x1, x2)
