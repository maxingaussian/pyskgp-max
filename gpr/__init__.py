"""
Created on Thu Sep 10 16:59:51 2015
@author: Max W. Y. Lam
"""

from .gpr import gpr as gpr
from .sgpr import sgpr as sgpr
from .cgpr import cgpr as cgpr
