"""
Created on Thu Oct 15 15:49:01 2015
@author: Max W. Y. Lam
"""

import numpy as np
from .gpr import gpr
from .sgpr import sgpr
from .regressor import regressor
from linalg.mat import mat
from optimizer.optimizer import optimizer


class hgpr(regressor):

    kern, gpr, U = None, None, None
    K, KInv, a, b, m = {}, {}, {}, {}, {}
    hierarchy, ulabels, hlabels, hlevels = None, None, None, -1
    fast_linalg, sparse_approx, sparse_method = False, True, None
    metric = None

    def __init__(self, kern, fast_linalg=False,
                 metric="manhattan", hlevels=-1,
                 sparse_approx=True, sparse_method="FITC",
                 rescale=True, normalize=False):
        super(hgpr, self).__init__(rescale, normalize)
        self.kern = kern
        self.metric = metric
        self.sparse_approx = sparse_approx
        self.sparse_method = sparse_method
        self.fast_linalg = fast_linalg
        self.hlevels = hlevels

    def fit(self, X, y, X_test=None):
        super(hgpr, self).fit(X, y, X_test)
        self.kern.fix_noise(np.std(y) * 0.1)
        if(X_test is None):
            self.hierarchical_samples(self.X)
        else:
            self.hierarchical_samples(np.vstack((self.X, self.Xs)))
        if(self.sparse_approx):
            self.gpr = sgpr(self.kern, fast_linalg=self.fast_linalg,
                            sparse_method=self.sparse_method,
                            sparse_size=self.hlevels, metric=self.metric,
                            U=self.U)
            self.gpr.N = self.N
            self.gpr.X = self.X
            self.gpr.y = self.y
            self.gpr.scaler = self.scaler
            self.gpr.normalizer = self.normalizer
        else:
            from sklearn.cluster import DBSCAN
            dbscan = DBSCAN(eps=1.).fit(self.X)
            mask = dbscan.core_sample_indices_
            self.gpr = gpr(self.kern)
            self.gpr.fit(X[mask], y[mask])
        self.cal()

    def predict(self, X, only_mu=True, transductive=False):
        super(hgpr, self).predict(X)
        slabels = self.hierarchy.predict(self.Xs)
        M = self.Xs.shape[0]
        mu = np.ones((M, 1))
        loglik = np.zeros((M, 1))
        for lab in set(np.unique(slabels)):
            inds = np.where(slabels == lab)[0]
            if(inds.shape[0] == 0):
                continue
            Xs = self.Xs[inds]
            mask = np.where(np.any(
                self.hlabels[:self.N] == lab, axis=1))[0]
            X = self.X[mask]
            Ksf = mat(self.kern.Ks(Xs, X))
            mu_gpr = None
            if(self.sparse_approx):
                mu_gpr = self.gpr.predict(Xs, True, transductive)[0]
            else:
                mu_gpr = self.gpr.predict(Xs, True)[0]
            mu[inds] = (mu_gpr + Ksf.dot(self.a[lab]).A)
            loglik[inds] += np.vdot(self.m[lab], self.a[lab])
            del Xs, X, Ksf
        return (mu, loglik)

    def predict_a(self, X, only_mu=True, transductive=False):
        super(hgpr, self).predict(X)
        slabels = self.hierarchy.predict(self.Xs)
        M = self.Xs.shape[0]
        mu = np.ones((M, 1))
        loglik = np.zeros((M, 1))
        for lab in set(np.unique(slabels)):
            inds = np.where(slabels == lab)[0]
            if(inds.shape[0] == 0):
                continue
            Xs = self.Xs[inds]
            mask = np.where(np.any(
                self.hlabels[:self.N] == lab, axis=1))[0]
            X = self.X[mask]
            Ksf = mat(self.kern.Ks(Xs, X))
            mu_gpr = None
            if(self.sparse_approx):
                mu_gpr = self.gpr.predict(Xs, True, transductive)[0]
            else:
                mu_gpr = self.gpr.predict(Xs, True)[0]
            mu[inds] = (mu_gpr + Ksf.dot(self.a[lab]).A)
            loglik[inds] += np.vdot(self.m[lab], self.a[lab])
            del Xs, X, Ksf
        return (mu, loglik)

    def predict_b(self, X, only_mu=True, transductive=False):
        super(hgpr, self).predict(X)
        slabels = self.hierarchy.predict(self.Xs)
        M = self.Xs.shape[0]
        mu = np.ones((M, 1))
        loglik = np.zeros((M, 1))
        for lab in set(np.unique(slabels)):
            inds = np.where(slabels == lab)[0]
            if(inds.shape[0] == 0):
                continue
            Xs = self.Xs[inds]
            mask = np.where(np.any(
                self.hlabels[:self.N] == lab, axis=1))[0]
            X = self.X[mask]
            Ksf = mat(self.kern.Ks(Xs, X))
            mu[inds] = (Ksf.dot(self.b[lab]).A)
            mask = np.where(np.any(self.hlabels[:self.N] == lab, axis=1))[0]
            y = self.y[mask]
            loglik[inds] += np.vdot(y, self.b[lab])
            del Xs, X, Ksf
        return (mu, loglik)

    def cal(self):
        self.gpr.kern.set_params(self.kern._params)
        self.gpr.cal()
        for lab in self.ulabels:
            mask = np.where(np.any(self.hlabels[:self.N] == lab, axis=1))[0]
            X = self.X[mask]
            y = self.y[mask]
            K = None
            K = mat(self.kern.K(X, noisy=True))
            mu = self.gpr.predict(X, True)[0]
            self.m.update({lab: y-mu})
            self.K.update({lab: K})
            self.a.update({lab: K.solve(self.m[lab]).A})
            self.b.update({lab: K.solve(y).A})
            del X, y, K

    def hierarchical_samples(self, H):
        from sklearn.mixture import VBGMM
        self.hierarchy = VBGMM(n_components=self.hlevels,
                               covariance_type='diag',
                               n_iter=100,
                               verbose=True).fit(H)
        self.hlabels = np.argsort(self.hierarchy.predict_proba(
            H), axis=1)[:, -2:]
        self.ulabels = set(np.unique(self.hlabels))
        self.U = self.hierarchy.means_

    def optimize_hyperparams(self, trials=3, method='ARBCDA', message=False):
        def initialize():
            self.kern.randomize_params()
            self.Z = None
            self.firstZ = marginal_likelihood(self.kern._params)
            return self.kern._params

        def marginal_likelihood(params):
            self.params_diff = self.kern._params - params
            self.kern.set_params(params)
            self.cal()
            if(self.Z is not None):
                self.lZ = self.Z
            self.Z = np.log(2*np.pi) * self.N
            for lab in self.ulabels:
                self.Z += (np.sum((self.m[lab])[:] *
                           self.a[lab][:]) +
                           self.K[lab].logdet())
            if(np.isnan(self.Z) or np.isinf(self.Z)):
                self.Z = 1e10
            if(self.Z < -100):
                self.Z = 10 * abs(self.Z)
            return self.Z

        def marginal_likelihood_jacobian(params, d, approx=False):
            dZ_dP = 0
            if(approx):
                dZ_dP = self.kern.rand.rand() * 1e-3 +\
                    (self.Z-self.lZ) * np.sign(self.params_diff[d]) /\
                    max(abs(self.params_diff[d]), 1e-5) * 10
            else:
                for lab in self.ulabels:
                    mask = np.where(np.any(self.hlabels[:self.N] == lab, axis=1))[0]
                    X = self.X[mask]
                    dK_dP = self.kern.dK_dP(X, d)
                    dZ_dP += mat(self.a[lab].T).dot(dK_dP)\
                        .dot(self.a[lab]).trace() -\
                        self.K[lab].solve(dK_dP).trace()
                    del X, dK_dP
                dZ_dP /= self.hlevels
            return dZ_dP
        self.opt = optimizer(method, initialize, marginal_likelihood,
                             marginal_likelihood_jacobian)
        self.kern.set_params(self.opt.run(trials, message))
        self.cal()

    def optimize_mean(self, folds=2, trials=2,
                      method='ARBCDA', message=False):
        self.gpr.optimize_hyperparams(folds, trials, method, message)
        self.kern.set_params(self.gpr.kern._params)
        self.cal()
