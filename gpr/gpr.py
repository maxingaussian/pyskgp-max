"""
Created on Tue Sep 15 13:11:24 2015
@author: Max W. Y. Lam

Prerequisite: python2.7/python3.4 & scikit-learn >=0.16.0
"""
import numpy as np
from .regressor import regressor
from linalg.mat import mat
from optimizer.optimizer import optimizer
from sklearn.externals.six.moves import xrange


class gpr(regressor):

    kern, K, KInv, a = None, None, None, None
    fast_linalg = False

    def __init__(self, kern, fast_linalg=False,
                 rescale=True, normalize=False):
        super(gpr, self).__init__(rescale, normalize)
        self.kern = kern
        self.fast_linalg = fast_linalg

    def fit(self, X, y, X_test=None):
        super(gpr, self).fit(X, y, X_test)
        self.cal()

    def predict(self, X, only_mu=False):
        super(gpr, self).predict(X)
        mu, var = None, None
        Ksf = mat(self.kern.Ks(self.Xs, self.X))
        mu = Ksf.dot(self.a).A
        if(not only_mu):
            Kss = self.kern.K(self.Xs, noisy=False)
            var = Kss - Ksf.dot(self.K.solve(Ksf.A.T).A).A
        return mu, var

    def cal(self):
        self.K = mat(self.kern.K(self.X), self.fast_linalg)
        self.a = self.K.solve(self.y).A

    def cal_marginal_likelihood(self, nrmse=True):
        from sklearn.metrics import mean_squared_error
        _X, _y = self.scaler.inverse_transform(self.X), self.y
        from sklearn.model_selection import KFold
        kf = KFold(n_folds=3)
        self.Z = 0
        for train, test in kf.split(_X):
            self.fit(_X[train], _y[train])
            mse = mean_squared_error(_y[test], self.predict(_X[test],
                                     True)[0])
            self.Z += mse/np.var(_y[test].ravel())
        self.fit(_X, _y)
        self.Z *= (np.sum(self.y[:] * self.a[:]) + self.K.logdet()\
            + self.N * np.log(2 * np.pi)+1e3)
        return self.Z

    def cal_marginal_likelihood_jacobian(self, d):
        self.cal()
        dK_dP = self.kern.dK_dP(self.X, d)
        dZ_dP = mat(self.a.T).dot(dK_dP).dot(self.a).trace() -\
            self.K.solve(dK_dP).trace()
        del dK_dP
        return dZ_dP

    def optimize_hyperparams(self, folds=3, trials=2,
                             method='COBYLA', message=False):
        def initialize():
            self.kern.randomize_params()
            self.Z = None
            self.firstZ = marginal_likelihood(self.kern._params)
            return self.kern._params

        def marginal_likelihood(params, nmse=True):
            self.params_diff = self.kern._params - params
            self.kern.set_params(params)
            if(self.Z is not None):
                self.lZ = self.Z
            if(nmse):
                from sklearn.metrics import mean_squared_error
                _X, _y = self.scaler.inverse_transform(self.X), self.y
                from sklearn.model_selection import KFold
                kf = KFold(n_folds=3)
                self.Z = 0
                for train, test in kf.split(_X):
                    self.fit(_X[train], _y[train])
                    mse = mean_squared_error(_y[test], self.predict(_X[test],
                                             True)[0])
                    self.Z += mse/np.var(_y[test].ravel())
                self.fit(_X, _y)
                self.Z *= (np.sum(self.y[:] * self.a[:]) + self.K.logdet()\
                    + self.N * np.log(2 * np.pi)+1e3)
                return self.Z
            self.cal()
            self.Z = np.sum(self.y[:] * self.a[:]) + self.K.logdet()\
                + self.N * np.log(2 * np.pi)
            if(np.isnan(self.Z) or np.isinf(self.Z)):
                self.Z = 1e10
            if(self.Z < -100):
                self.Z = 10 * abs(self.Z)
            return self.Z

        def marginal_likelihood_jacobian(params, d, approx=False):
            dZ_dP = 0
            if(approx):
                dZ_dP = self.kern.rand.rand() * 1e-3 +\
                    (self.Z-self.lZ) * np.sign(self.params_diff[d]) /\
                    max(abs(self.params_diff[d]), 1e-5)
            else:
                dK_dP = self.kern.dK_dP(self.X, d)
                dZ_dP = mat(self.a.T).dot(dK_dP).dot(self.a).trace() -\
                    self.K.solve(dK_dP).trace()
                del dK_dP
            return dZ_dP
        self.opt = optimizer(method, initialize, marginal_likelihood,
                             marginal_likelihood_jacobian)
        if(folds == 1):
            best_params = self.opt.run(trials, message)
            self.kern.set_params(best_params)
            return
        from sklearn.metrics import mean_squared_error
        _X, _y = self.scaler.inverse_transform(self.X), self.y
        params = np.zeros(self.kern._params.shape[0])
        sum_weights = 0
        t = 0
        from sklearn.model_selection import KFold
        kf = KFold(n_folds=folds, shuffle=True,
                   random_state=np.random.randint(1001))
        for train, test in kf.split(_X):
            self.fit(_X[train], _y[train])
            best_params = self.opt.run(trials, message)
            self.kern.set_params(best_params)
            nrmse = mean_squared_error(_y[test], self.predict(_X[test],
                                     True)[0])/np.var(_y[test])
            print("K-fold Cross validation", (t+1), "=", nrmse)
            print(best_params, "\n")
            params += best_params / (nrmse)
            sum_weights += 1./(nrmse)
            t += 1
        params /= sum_weights
        print("\nOptimization Best Params:")
        print(params)
        self.fit(_X, _y)
        self.kern.set_params(params)
        self.cal()
