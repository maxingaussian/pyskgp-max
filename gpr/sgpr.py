"""
Created on Thu Oct 15 15:48:47 2015
@author: Max W. Y. Lam

Prerequisite: python2.7/python3.4 & scikit-learn >=0.16.0
"""
import numpy as np
from .regressor import regressor
from linalg.mat import mat
from optimizer.optimizer import optimizer
from sklearn.externals.six.moves import xrange


class sgpr(regressor):

    kern = None
    K, KInv, ldetK, Ku, Kuf = None, None, None, None, None
    a, U, B, DInv = None, None, None, None
    fast_linalg = False
    sparse_method, metric = None, None

    def __init__(self, kern, fast_linalg=False,
                 sparse_method="DIC", sparse_size=100,
                 metric="euclidean", U=None,
                 rescale=True, normalize=False):
        super(sgpr, self).__init__(rescale, normalize)
        self.kern = kern
        self.sparse_method = sparse_method
        self.sparse_size = sparse_size
        self.metric = metric
        self.U = U
        self.fast_linalg = fast_linalg

    def fit(self, X, y, X_test=None):
        super(sgpr, self).fit(X, y, X_test)
        if(self.U is None):
            self.sparse_size = min(self.sparse_size, self.N/2)
            if(X_test is None):
                self.sparse_samples(self.X)
            else:
                self.sparse_samples(np.vstack((self.X, self.Xs)))
        else:
            self.U = self.scaler.transform(self.U)
        self.cal()

    def predict(self, X, only_mu=False, transductive=False):
        super(sgpr, self).predict(X)
        var = None
        Ksu = mat(self.kern.Ks(self.Xs, self.U))
        mu = Ksu.dot(self.B.solve(self.Kuf.A).A).dot(
            self.DInv).dot(self.y).A
        if(not only_mu):
            partial = ["DIC", "FIC", "PIC"]
            Kss = None
            if(self.sparse_method in partial):
                Kss = Ksu.dot(self.Ku.solve(Ksu.A.T).A).A
            else:
                Kss = self.kern.K(self.Xs, noisy=False)
            var = Kss - Ksu.dot(self.Ku.inv().A-self.B.inv().A).dot(Ksu.A.T).A
            del Kss
        del Ksu
        return mu, var

    def cal(self):
        self.Ku = mat(self.kern.K(self.U, noisy=True))
        self.Kuf = mat(self.kern.Ks(self.U, self.X))
        self.K = mat(self.Kuf.A.T.dot(self.Ku.solve(self.Kuf.A).A))
        ldetDInv = 0
        if(self.sparse_method == "DIC" or self.sparse_method == "DTC"):
            self.DInv = np.eye(self.N) / self.kern.noise
            ldetDInv -= self.N * np.log(max(self.kern.noise, 1e-5))
        elif(self.sparse_method == "FIC" or self.sparse_method == "FITC"):
            self.DInv = np.eye(self.N)
            for i in xrange(self.N):
                D_ii = (self.kern.noise + (self.kern.k(
                    self.X[i, :], self.X[i, :]) - self.K.A[i, i]))
                self.DInv[i, i] /= D_ii
                ldetDInv -= np.log(max(D_ii, 1e-5))
        elif(self.sparse_method == "PIC" or self.sparse_method == "PITC"):
            self.DInv = np.eye(self.N) * self.kern.noise
            block_size = self.N/self.sparse_size
            for i in xrange(self.N/block_size+1):
                flag = False
                block_i = np.eye(block_size)
                for j in xrange(i*block_size, (i+1)*block_size):
                    if(j >= self.N):
                        flag = True
                        break
                    block_i[j % block_size, j % block_size] += self.kern.k(
                        self.X[j, :], self.X[j, :]) - self.K.A[j, j]
                    for k in xrange(j+1, (i+1)*block_size):
                        if(k >= self.N):
                            flag = True
                            break
                        K_jk = self.kern.k(self.X[j, :], self.X[k, :])
                        K_jk -= self.K.A[j, k]
                        block_i[j % block_size, k % block_size] = K_jk
                        block_i[k % block_size, j % block_size] = K_jk
                else:
                    block_i = mat(block_i)
                    self.DInv[i*block_size:(i+1)*block_size,
                              i*block_size:(i+1)*block_size] =\
                        block_i.inv().A
                    ldetDInv -= block_i.logdet()
                if(flag):
                    last_block_size = self.N - i*block_size
                    block_i = mat(block_i[:last_block_size, :last_block_size])
                    if(last_block_size > 0):
                        self.DInv[i*block_size:self.N,
                                  i*block_size:self.N] =\
                            block_i.inv().A
                        ldetDInv -= block_i.logdet()
        self.B = mat(self.Ku.A + self.Kuf.dot(self.DInv).dot(self.Kuf.A.T).A,
                     approx=False, sym_pos=False)
        temp_mat = mat(np.eye(self.N) -
                       mat(self.DInv).dot(
                       self.Kuf.A.T).dot(self.B.solve(self.Kuf.A).A).A)
        self.KInv = temp_mat.dot(self.DInv)
        self.a = temp_mat.dot(self.DInv.dot(self.y))
        self.ldetK = self.B.logdet() - self.Ku.logdet() - ldetDInv
        del temp_mat

    def sparse_samples(self, H):
        from sklearn.cluster import AgglomerativeClustering
        agglo = AgglomerativeClustering(n_clusters=self.sparse_size,
                                        compute_full_tree=True,
                                        affinity=self.metric,
                                        linkage='complete',
                                        pooling_func=np.mean)
        labels = agglo.fit_predict(H)
        self.U = np.zeros((len(set(labels)), H.shape[1]))
        for i, lab in enumerate(set(labels)):
            h = H[labels == lab]
            mu = np.median(h, axis=0)
            self.U[i] = mu
        del agglo

    def cal_marginal_likelihood(self, nrmse=True):
        from sklearn.metrics import mean_squared_error
        _X, _y = self.scaler.inverse_transform(self.X), self.y
        from sklearn.model_selection import KFold
        kf = KFold(n_folds=3, shuffle=True,
                   random_state=np.random.randint(1001))
        self.Z = 0
        for train, test in kf.split(_X):
            self.fit(_X[train], _y[train])
            mse = mean_squared_error(_y[test], self.predict(_X[test],
                                     True)[0])
            self.Z += mse/np.var(_y[test].ravel())
        self.fit(_X, _y)
        self.Z *= (np.sum(self.y[:] * self.a.A[:]) + self.ldetK\
            + self.N * 2 * np.pi + 1e3)
        return self.Z

    def cal_marginal_likelihood_jacobian(self, d):
        self.cal()
        dKu = self.kern.dK_dP(self.U, d)
        dKuf = self.kern.dKs_dP(self.U, self.X, d)
        dKuInv = self.Ku.solve(dKu).dot(self.Ku.solve(self.Kuf.A).A).A
        dK_dP = mat(self.Kuf.A.T).dot(2*self.Ku.solve(dKuf).A -
                                      dKuInv)
        del dKu, dKuf, dKuInv
        dZ_dP = mat(self.a.A.T).dot(dK_dP.dot(self.a.A).A).trace() -\
            self.KInv.dot_trace(dK_dP.A)
        return dZ_dP

    def optimize_hyperparams(self, folds=3, trials=2,
                             method='COBYLA', message=False):
        def initialize():
            self.kern.randomize_params()
            self.Z = None
            self.firstZ = marginal_likelihood(self.kern._params)
            return self.kern._params

        def marginal_likelihood(params, mse=False):
            self.params_diff = self.kern._params - params
            self.kern.set_params(params)
            if(self.Z is not None):
                self.lZ = self.Z
            if(mse):
                from sklearn.metrics import mean_squared_error
                _X, _y = self.scaler.inverse_transform(self.X).copy(), self.y.copy()
                from sklearn.model_selection import KFold
                kf = KFold(n_folds=3, shuffle=True,
                           random_state=np.random.randint(1001))
                self.Z = 0
                for train, test in kf.split(_X):
                    self.fit(_X[train], _y[train])
                    mse = mean_squared_error(_y[test], self.predict(_X[test],
                                             True)[0])
                    self.Z += mse/np.var(_y[test].ravel())
                self.fit(_X, _y)
                self.Z *= (np.sum(self.y[:] * self.a.A[:]) + self.ldetK\
                    + self.N * 2 * np.pi + 1e3)
                return self.Z
            self.cal()
            self.Z = np.sum(self.y[:] * self.a.A[:]) + self.ldetK\
                + self.N * 2 * np.pi
            if(np.isnan(self.Z) or np.isinf(self.Z)):
                self.Z = 1e10
            if(self.Z < 0):
                self.Z = 10 * abs(self.Z)
            return self.Z

        def marginal_likelihood_jacobian(params, d, approx=False):
            dZ_dP = 0
            if(approx):
                dZ_dP = self.kern.rand.rand() * 1e-3 +\
                    (self.Z-self.lZ) * np.sign(self.params_diff[d]) /\
                    max(abs(self.params_diff[d]), 1e-5)
            else:
                dKu = self.kern.dK_dP(self.U, d)
                dKuf = self.kern.dKs_dP(self.U, self.X, d)
                dKuInv = self.Ku.solve(dKu).dot(self.Ku.solve(self.Kuf.A).A).A
                dK_dP = mat(self.Kuf.A.T).dot(2*self.Ku.solve(dKuf).A -
                                              dKuInv)
                del dKu, dKuf, dKuInv
                dZ_dP = mat(self.a.A.T).dot(dK_dP.dot(self.a.A).A).trace() -\
                    self.KInv.dot_trace(dK_dP.A)
            return dZ_dP
        self.opt = optimizer(method, initialize, marginal_likelihood,
                             marginal_likelihood_jacobian)
        if(folds == 1):
            best_params = self.opt.run(trials, message)
            self.kern.set_params(best_params)
            return
        from sklearn.metrics import mean_squared_error
        _X, _y = self.scaler.inverse_transform(self.X), self.y
        params = np.zeros(self.kern._params.shape[0])
        sum_weights = 0
        t = 0
        from sklearn.model_selection import KFold
        kf = KFold(n_folds=folds, shuffle=True,
                   random_state=np.random.randint(1001))
        for train, test in kf.split(_X):
            self.fit(_X[train], _y[train])
            best_params = self.opt.run(trials, message)
            self.kern.set_params(best_params)
            nrmse = mean_squared_error(_y[test], self.predict(_X[test],
                                     True)[0])/np.var(_y[test])
            print("K-fold Cross validation", (t+1), "=", nrmse)
            print(best_params, "\n")
            params += best_params / (nrmse)
            sum_weights += 1./(nrmse)
            t += 1
        params /= sum_weights
        print("\nOptimization Best Params:")
        print(params)
        self.fit(_X, _y)
        self.kern.set_params(params)
        self.cal()
