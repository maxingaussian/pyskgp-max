"""
Created on Thu Oct 15 16:19:01 2015
@author: Max W. Y. Lam
"""
import abc
import numpy as np
from sklearn.preprocessing import StandardScaler, Normalizer


class regressor(object):

    scaler, normalizer = None, None
    X, Xs, y, N = None, None, None, None

    def __init__(self, rescale=True, normalize=False):
        if(normalize):
            self.normalizer = Normalizer(copy=False)

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def fit(self, X, y, X_test=None):
        self.scaler = StandardScaler(copy=True).fit(X)
        self.X = self.scaler.transform(X)
        if(self.normalizer is not None):
            self.X = self.normalizer.fit_transform(self.X)
        if(not self.kern.setted):
            self.kern.__init__(self.X.shape[1], self.kern.ard)
            self.kern.setted = True
        self.Xs = None
        if(X_test is not None):
            self.Xs = self.scaler.transform(X_test)
            if(self.normalizer is not None):
                self.Xs = self.normalizer.fit_transform(self.Xs)
        self.y = y
        self.N = X.shape[0]

    @abc.abstractmethod
    def predict(self, X, only_mu=False):
        self.Xs = self.scaler.transform(X)
        if(self.normalizer is not None):
            self.Xs = self.normalizer.fit_transform(self.Xs)

    def plot(self, true_func=None, show_data=True, show_sample_paths=10):
        pts = 700
        errors = [0.13, 0.25, 0.39, 0.52, 0.67, 0.84, 1.04, 1.28, 1.64, 2.2]
        _X = self.scaler.inverse_transform(self.X)
        grows = np.ceil(np.sqrt(_X.shape[1]))
        from matplotlib import pyplot as plt
        fig = plt.figure(facecolor='black')
        for d in range(_X.shape[1]):
            ax = plt.subplot(grows, grows, d + 1, axisbg='black')
            xrng = _X[:, d].max() - _X[:, d].min()
            Xs = np.tile(np.linspace(_X[:, d].min() - 0.3*xrng,
                         _X[:, d].max() + 0.3*xrng, pts)[:, None], _X.shape[1])
            mu, var = self.predict(Xs)
            mu = mu.ravel()
            sigma = np.sqrt(var.diagonal())
            for er in errors:
                ax.fill_between(Xs[:, d], mu - er * sigma, mu + er * sigma,
                                alpha=((3 - er)/5.)**1.7, facecolor='white',
                                linewidth=0.0)
            ax.plot(Xs[:, d], mu[:], 'red', linewidth=1.5)
            for _ in range(show_sample_paths):
                z = np.random.multivariate_normal(mu, var)
                ax.plot(Xs[:, d], z[:], linewidth=1.0)
            if(true_func):
                ax.plot(Xs[:, d], true_func(Xs)[:], 'r--')
            if(show_data):
                ax.errorbar(_X[:, d], self.y.ravel(), fmt='r.', markersize=10)
            yrng = self.y.max() - self.y.min()
            plt.ylim(self.y.min() - 0.5*yrng, self.y.max() + 0.5*yrng)
            plt.xlim(Xs[:, d].min(), Xs[:, d].max())
            del mu, var, Xs
        del _X
        plt.show()
