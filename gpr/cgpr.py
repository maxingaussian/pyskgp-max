"""
Created on Mon Oct 26 16:45:48 2015
@author: Max W. Y. Lam

Prerequisite: python2.7/python3.4 & scikit-learn >=0.16.0
"""
import numpy as np
import kernels
from .gpr import gpr
from .sgpr import sgpr
from sklearn.preprocessing import StandardScaler
from sklearn.externals.six.moves import xrange


class cgpr(object):

    """
    Clustered 2-layer Sparse Gaussian Process Regression
    """

    X, y, m, N, D, layer = None, None, None, -1, -1, 0
    Xc, yc, save_rmse = None, None, -1
    cen_alg, cmp_alg = None, None
    mu_kern, thred = None, 150
    x_scaler, y_scaler = None, None

    def __init__(self, mu_kern=kernels.se(), thred=100, layer=0,
                 _cen_alg="mean", _cmp_alg="canberra"):
        self._cen_alg = _cen_alg
        self._cmp_alg = _cmp_alg
        self.mu_kern = mu_kern
        self.thred = thred
        self.layer = layer

    def fit(self, X_train, y_train):
        self.x_scaler = StandardScaler(copy=True).fit(X_train)
        self.y_scaler = StandardScaler(copy=True).fit(y_train)
        self.X = self.x_scaler.transform(X_train).copy()
        self.y = self.y_scaler.transform(y_train).copy()
        self.thred *= 5
        if(self.thred < X_train.shape[0] * 0.8):
            self.X, self.y = self._find_centers(self.X.copy(), self.y.copy())
        self.N, self.D = self.X.shape
        print("Training Data Size =", self.N)
        self.thred /= 5
        self.Xc, self.yc = self._find_centers(self.X, self.y)
        print("Sparse Data Size =", self.Xc.shape[0])
        self.gpr = [None]
        self.gpr[0] = sgpr(kern=self.mu_kern, U=self.Xc)
        self.gpr[0].kern.setted = True
        self.gpr[0].kern.randomize_params()
        self.gpr[0].fit(self.X.copy(), self.y.copy())
        print("(Mean Regressoin) Train hyperparamters")
        self.gpr[0].optimize_hyperparams(3, 2)
        self._residual_gpr_fit()

    def predict(self, X_test):
        X = self.x_scaler.transform(X_test.copy())
        pred = sum([self.gpr[i].predict(X, True)[0] for i in xrange(len(self.gpr))])
        return self.y_scaler.inverse_transform(pred)

    def plot_centers(self):
        if(self.D != 1):
            print("Plot function not supported for multi-dimensions!")
            return
        from matplotlib import pylab as pb
        pb.figure()
        pb.plot(self.X.ravel(), self.y.ravel(), 'b.')
        pb.plot(self.Xc.ravel(), self.yc.ravel(), 'r.', markersize=15)

    def plot(self, true_func=None, data=True):
        pts = 500
        _X = self.x_scaler.inverse_transform(self.X)
        _y = self.y_scaler.inverse_transform(self.y)
        grows = np.ceil(np.sqrt(_X.shape[1]))
        from matplotlib import pyplot as plt
        plt.figure()
        for d in xrange(_X.shape[1]):
            ax = plt.subplot(grows, grows, d + 1)
            xrng = _X[:, d].max() - _X[:, d].min()
            Xs = np.tile(np.linspace(_X[:, d].min() - 0.3*xrng,
                         _X[:, d].max() + 0.3*xrng, pts)[:, None], _X.shape[1])
            mu = self.predict(Xs)
            mu = mu.ravel()
            ax.plot(Xs[:, d], mu[:], 'black')
            if(true_func):
                ax.plot(Xs[:, d], true_func(Xs)[:], 'r--')
            if(data):
                ax.errorbar(_X[:, d], _y.ravel(), fmt='r.', markersize=10)
            yrng = _y.max() - _y.min()
            plt.ylim(_y.min() - 0.5*yrng, _y.max() + 0.5*yrng)
            plt.xlim(Xs[:, d].min(), Xs[:, d].max())
        del _X, _y
        plt.show()

    def _residual_gpr_fit(self):
        m = self.y.copy()
        for i in xrange(self.layer):
            m -= self.gpr[-1].predict(self.X, True)[0]
            self.X_c, self.y_c = self._find_centers(self.X.copy(), m.copy())
            self.gpr.append(gpr(kernels.se()))
            self.gpr[-1].fit(self.X_c.copy(), self.y_c.copy())
            self.gpr[-1].optimize_hyperparams(3, 2)

    def _merge(self, S1, S2):
        S = np.vstack((S1, S2))
        C = self.__merge(S, self.cmp_alg(self.cen_alg(S1),
                         self.cen_alg(S2))/self.thred*1e-1)
        if(C.shape[1] > 1):
            return C[:, :-1], C[:, -1][:, None]
        return C[:, 0][:, None], C[:, 1][:, None]

    def __merge(self, C, thred):
        redun = []
        for i in xrange(C.shape[0]-1):
            for j in xrange(i+1, C.shape[0]):
                if(j in redun):
                    continue
                if(self.cmp_alg(C[i, :], C[j, :]) < thred/self.D):
                    C[i, :] = (C[i, :]+C[j, :])/2.
                    redun.append(j)
        return np.delete(C, redun, 0)

    def _find_ends(self, S):
        smid_pt = self.cen_alg(S)
        end1 = S[np.argmax([self.cmp_alg(S[i, :-1], smid_pt[:-1])
                            for i in xrange(S.shape[0])])]
        end2 = S[np.argmax([self.cmp_alg(S[i, :-1], end1[:-1])
                            for i in xrange(S.shape[0])])]
        return end1, end2

    def _find_centers(self, X, y):
        tree, size = {}, {}
        S = np.hstack((X, y))
        num = S.shape[0]
        occupy = [i for i in xrange(num)]
        node = 0
        C = None
        while(True):
            if(node == 0):
                max_pt, min_pt = self._find_ends(S)
                ti = np.asarray(occupy)
            else:
                node = max(size, key=size.get)
                occupy.append(node)
                ti, min_pt, max_pt = tree[node]
            tS = S[ti].copy()
            cen_pt = self.cen_alg(tS)
            cmp_dis = np.asarray([self.cmp_alg(tS[i, :-1], cen_pt[:-1])
                                  for i in xrange(tS.shape[0])])
            ci = np.argsort(cmp_dis)
            li = ti[ci[:ti.shape[0]/2]]
            size[node] = li.shape[0]
            tree[node] = (li, min_pt, cen_pt)
            occupy.remove(node)
            node = min(occupy)
            ri = ti[ci[ti.shape[0]/2:]]
            size[node] = ri.shape[0]
            tree[node] = (ri, cen_pt, max_pt)
            occupy.remove(node)
            node = min(occupy)
            if(len(tree) > self.thred):
                break
        for k, (ti, l_pt, r_pt) in tree.items():
            tS = S[ti]
            cen_pt = self.cen_alg(tS)
            mid_pt = tS[np.argmin([self.cmp_alg(tS[i, :], cen_pt)
                                  for i in xrange(tS.shape[0])])]
            if(C is None):
                C = cen_pt[None, :]
            else:
                C = np.vstack((C, mid_pt))
        del S, tree, size
        if(C.shape[1] > 1):
            return C[:, :-1], C[:, -1][:, None]
        return C[:, 0][:, None], C[:, 1][:, None]

    def _detect_param_d(self, gpr_i, X, y, nrmse=True):
        def measure_fitness(gpr_i, params, X, y, nrmse):
            self.gpr[gpr_i].kern.set_params(params)
            self.gpr[gpr_i].cal_marginal_likelihood(nrmse)
            return self.gpr[gpr_i].Z
        def find_most_reactive(gpr_i, X, y, counter, nrmse, lds):
            max_diff, max_diff_d = -1, 0
            self.gpr[gpr_i].kern.set_params(self.best_params)
            for d in xrange(self.gpr[gpr_i].kern.n):
                if(d in lds):
                    continue
                nrmse_diff = abs(self.gpr[gpr_i].cal_marginal_likelihood_jacobian(d))/(counter[d]*2+1.)
                if(nrmse_diff > max_diff):
                    max_diff = nrmse_diff
                    max_diff_d = d
            return max_diff_d
        self.gpr[gpr_i].fit(X, y)
        self.gpr[gpr_i].optimize_hyperparams(2, 3)
        self.best_params = self.gpr[gpr_i].kern._params
        self.gpr[gpr_i].cal_marginal_likelihood()
        self.best_nrmse = self.gpr[gpr_i].Z
        return
        counter = [0 for _ in xrange(self.gpr[gpr_i].kern.n)]
        no_change = 0
        d, lds = 0, []
        c = 0
        while True:
            c += 1
            lds.append(d)
            if(len(lds) > self.gpr[gpr_i].kern.n/2):
                lds.pop(0)
            d = find_most_reactive(gpr_i, X, y, counter, nrmse, lds)
            counter[d] += 1
            nrmse = 1e10
            params = np.abs(self.best_params).copy()
            print("\nNumber", c, "Detect params", d, " ... ...\n")
            sign = np.sign(self.gpr[gpr_i].cal_marginal_likelihood_jacobian(d))
            if(sign > 0):
                lp = params[d]/(2.**(3*np.random.rand()+2))
                rp = params[d]*2.
            else:
                lp = params[d]/2.
                rp = params[d]*(2.**(2*np.random.rand()+2))
            _best = 1e10
            ttt, ttc = 5, 0
            while True:
                ttc += 1
                midp = (lp+rp)/2.
                params[d] = (midp+lp)/2.
                if(d == 0):
                    l_nrmse = measure_fitness(gpr_i, params, X, y, True)
                else:
                    l_nrmse = measure_fitness(gpr_i, params, X, y, nrmse)
                if(l_nrmse < _best):
                    _best = l_nrmse
                    if(_best < self.best_nrmse[d]):
                        self.best_nrmse[d] = _best
                        self.best_params[d] = params[d]
                    if(d != 0 and _best < min(self.best_nrmse[1:])):
                        self.best_params = params.copy()
                params[d] = (midp+rp)/2.
                if(d == 0):
                    r_nrmse = measure_fitness(gpr_i, params, X, y, True)
                else:
                    r_nrmse = measure_fitness(gpr_i, params, X, y, nrmse)
                if(r_nrmse < _best):
                    _best = r_nrmse
                    if(_best < self.best_nrmse[d]):
                        self.best_nrmse[d] = _best
                        self.best_params[d] = params[d]
                    if(d != 0 and _best < min(self.best_nrmse[1:])):
                        self.best_params = params.copy()
                last_nrmse = nrmse
                nrmse = min(l_nrmse, r_nrmse)
                if(nrmse < last_nrmse and ttc == ttt):
                    ttt += 1
                print("mid:", midp, "nrmse:", nrmse, "best:", self.best_nrmse[d])
                if(l_nrmse > r_nrmse):
                    lp = midp
                    rp = (midp+rp)/2.
                else:
                    lp = (midp+lp)/2.
                    rp = midp
                if(ttc == ttt):
                    break
            if(_best > self.best_nrmse[d]):
                no_change += 1
                if(no_change > self.gpr[gpr_i].kern.n):
                    break
            else:
                no_change = max(no_change-1, 0)
        self.gpr[gpr_i].kern.set_params(self.best_params)

    def _detect_kernel(self, gpr_i, X, y, mul_term=True):
        kerns_l = [kernels.se(self.D, False), kernels.rq(self.D, False)]
        kerns_r = [kernels.lin(self.D, False), kernels.per(self.D, False)]
        mu_kerns_ = {}
        for i, kl in enumerate(kerns_l):
            kern = kl
            self.gpr[gpr_i] = gpr(kern)
            self.gpr[gpr_i].kern.setted = True
            self.gpr[gpr_i].kern.randomize_params()
            self.gpr[gpr_i].fit(X, y)
            params_init = self.gpr[gpr_i].kern._params
            self.best_nrmse, self.best_params = 1e10, np.abs(params_init).copy()
            print("\n (Kernel Detection) Train hyperparamters")
            self._detect_param_d(gpr_i, X.copy(), y.copy())
            kern.set_params(self.best_params)
            print("\tKern:", kern.name() + "\tNMSE:", self.best_nrmse)
            mu_kerns_.update({kern: self.best_nrmse})
            self.mu_kern = min(mu_kerns_, key=mu_kerns_.get)
            print("\n\tBest Mean Kernel:", self.mu_kern.name() +\
                "\tNMSE:", min(mu_kerns_.values()))
            if(mul_term):
                for kr in kerns_r:
                    kern = kl * kr
                    self.gpr[gpr_i] = gpr(kern)
                    self.gpr[gpr_i].kern.setted = True
                    self.gpr[gpr_i].kern.randomize_params()
                    self.gpr[gpr_i].fit(X, y)
                    params_init = self.gpr[gpr_i].kern._params
                    self.best_nrmse, self.best_params = 1e10, np.abs(params_init).copy()
                    print("\n (Kernel Detection) Train hyperparamters")
                    self._detect_param_d(gpr_i, X.copy(), y.copy())
                    kern.set_params(self.best_params)
                    print("\tKern:", kern.name() + "\tNMSE:", self.best_nrmse)
                    mu_kerns_.update({kern: self.best_nrmse})
                    self.mu_kern = min(mu_kerns_, key=mu_kerns_.get)
                    print("\n\tBest Mean Kernel:", self.mu_kern.name() +\
                        "\tNMSE:", min(mu_kerns_.values()))
        self.mu_kern.ard = False
        self.mu_kern.setted = True

    def save(self, path, rmse):
        save_pack = [self.thred, self.X, self.y, self.x_scaler, self.y_scaler,
                     self.gpr[0].kern, self.Xc]
        save_pack.append([rmse])
        if(self.layer > 0):
            save_pack.extend([self.gpr[1].X, self.gpr[1].y])
            for gpr_i in self.gpr[1:]:
                save_pack.append(gpr_i.kern)
        import pickle
        with open(path, "wb") as save_f:
            pickle.dump(save_pack, save_f, pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        import pickle
        with open(path, "rb") as load_f:
            load_pack = pickle.load(load_f)
            self.thred = load_pack[0]
            self.X, self.y = load_pack[1:3]
            self.x_scaler, self.y_scaler = load_pack[3:5]
            self.N, self.D = self.X.shape
            self.mu_kern, self.Xc = load_pack[5:7]
            self.gpr = [sgpr(self.mu_kern, U=self.Xc)]
            self.gpr[0].fit(self.X.copy(), self.y.copy())
            if(len(load_pack) > 7):
                if(isinstance(load_pack[7], list)):
                    self.save_rmse = np.double(load_pack[-1][0])
                else:
                    self.save_rmse = -1
            if(len(load_pack) > 8):
                self.X_c, self.y_c = load_pack[7:9]
                for i, kern_i in enumerate(load_pack[9:]):
                    self.gpr.append(gpr(kern_i))
                    self.gpr[i+1].fit(self.X_c.copy(), self.y_c.copy())

    def cen_alg(self, X):
        if(self._cen_alg == "arithmetic mean" or self._cen_alg == "mean"):
            return np.mean(X, axis=0)
        if(self._cen_alg == "interquartile mean" or self._cen_alg == "iqmean"):
            lq = np.percentile(X, 25, axis=0)
            rq = np.percentile(X, 75, axis=0)
            midq = 0.5*lq + 0.5*rq
            dis = self.cmp_alg(lq, midq)
            res = lq + rq
            count = 2
            for i in xrange(X.shape[0]):
                if(self.cmp_alg(X[i], midq) < dis):
                    res += X[i]
                    count += 1
            return res*1./count
        if(self._cen_alg == "median"):  
            return np.median(X, axis=0)

    def cmp_alg(self, x1, x2):
        if(self._cmp_alg == "norm0.1"):
            return np.linalg.norm(x1-x2, ord=0.1)
        if(self._cmp_alg == "norm0.5"):
            return np.linalg.norm(x1-x2, ord=0.5)
        if(self._cmp_alg == "norm1"):
            return np.linalg.norm(x1-x2, ord=1)
        if(self._cmp_alg == "norm2"):
            return np.linalg.norm(x1-x2, ord=2)
        if(self._cmp_alg == "norm_inf"):
            return np.max(np.abs(x1-x2))
        if(self._cmp_alg == "canberra"):
            return np.sum(np.abs(x1-x2)/(np.abs(x1)+np.abs(x2)))
