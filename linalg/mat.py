"""
Created on Thu Oct 15 18:24:57 2015
@author: Max W. Y. Lam
"""

import scipy as sp
from sklearn.utils import extmath


class mat(object):

    A, approx, sym_pos = None, None, True
    _logdet, _AInv = None, None

    def __init__(self, A, approx=False, sym_pos=True):
        self.A = A
        self.sym_pos = sym_pos
        if(approx):
            try:
                from hodlr import HODLR
                self.approx = HODLR(A.copy(), A.diagonal().copy(),
                                    min(max(50, A.shape[0]/20),
                                    A.shape[0]/2), 1e-16)
            except:
                print("""Warning: HODLR is not installed.
                         Changed to normal solver!""")

    def dot(self, B, approx=False):
        if(self.approx is not None):
            return mat(self.approx.matrix_product(B), approx)
        return mat(extmath.fast_dot(self.A, B), approx)

    def solve(self, b, approx=False):
        if(self.approx is not None):
            return mat(self.approx.solve(b), approx)
        if(self._AInv is None):
            self._AInv = mat(extmath.linalg.solve(self.A,
                                                  sp.eye(self.A.shape[0]),
                                                  sym_pos=self.sym_pos))
        return self._AInv.dot(b)

    def inv(self, approx=False):
        if(self._AInv is None):
            if(self.approx is not None):
                self._AInv = mat(self.approx.solve(sp.eye(self.A.shape[0])),
                                 approx)
            else:
                self._AInv = mat(extmath.linalg.solve(self.A,
                                                      sp.eye(self.A.shape[0]),
                                                      sym_pos=self.sym_pos),
                                 approx)
        return self._AInv

    def logdet(self):
        if(self._logdet is None):
            if(self.approx is not None):
                self._logdet = self.approx.logdet()
            else:
                self._logdet = extmath.fast_logdet(self.A)
        return self._logdet

    def trace(self):
        return self.A.diagonal().sum()

    def dot_trace(self, B):
        return sp.einsum('ij,ji->', self.A, B)
