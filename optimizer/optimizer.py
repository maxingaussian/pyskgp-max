"""
Created on Sun Sep 20 16:41:45 2015
@author: Max W. Y. Lam
"""

import sys
import numpy as np
from sklearn.externals.six.moves import xrange


class optimizer(object):

    method = ""
    init_func, obj_func, jac_func = None, None, None
    tol = 0
    iters = 0
    min_obj, best_x = 1e10, None
    n, delta, delta_x, ldelta_x = -1, None, None, None
    delta_init, delta_min, delta_max = 1., 1e-3, 50.
    eta_min, eta_max = 0.5, 1.2
    funv, lfunv = 0, None
    jacv, ljacv = None, None

    def __init__(self, method, init_func, obj_func, jac_func, tol=1e-12):
        self.method = method
        self.init_func = init_func
        self.obj_func = obj_func
        self.jac_func = jac_func
        self.tol = tol

    def run(self, trials, message=False):

        def obj_func_wrapper(x):
            self.iters += 1
            x = np.abs(x)
            self.funv = self.obj_func(x)
            if(self.iters == 1):
                self.best_x = x.copy()
                self.min_obj = self.funv
                if(not message):
                    sys.stdout.write(".")
                else:
                    sys.stdout.write("(%d, %.5f, %.5f)" %
                                     (self.iters, self.min_obj, self.funv))
                    print("\n", x)
            else:
                if(not message):
                    sys.stdout.write(".")
                else:
                    sys.stdout.write("\n(%d, %.5f, %.5f)" %
                                     (self.iters, self.min_obj, self.funv))
                    print("\n", x)
            sys.stdout.flush()
            if(self.funv < self.min_obj):
                self.min_obj = self.funv
                self.best_x = x.copy()
            return self.funv

        def jac_func_wrapper(x):
            x = np.abs(x)
            for d in xrange(self.n):
                jac_func_d_wrapper(x, d)
            return self.delta_x

        def jac_func_d_wrapper(x, d, approx=False):
            self.jacv[d] = self.jac_func(x, d, approx)
            cs = self.ljacv[d] * self.jacv[d]
            if(cs > 0):
                self.delta[d] = min(self.delta[d]*self.eta_max,
                                    self.delta_max)
                self.delta_x[d] = np.sign(self.jacv[d])*self.delta[d]
                self.ldelta_x[d] = self.delta_x[d]
            elif(cs < 0):
                self.delta[d] = max(self.delta[d]*self.eta_min,
                                    self.delta_min)
                if(self.funv > self.lfunv[d]):
                    self.delta_x[d] = -self.ldelta_x[d]
            else:
                self.delta_x[d] = np.sign(self.jacv[d])*self.delta[d]
                self.ldelta_x[d] = self.delta_x[d]
            if(self.iters > 2):
                self.delta[d] *= abs(self.jacv[d]*self.jacv[d]/self.ljacv[d]) ** 0.2
            self.ljacv[d] = self.jacv[d]
            self.lfunv[d] = self.funv

        from sklearn.utils import check_random_state
        self.rand = check_random_state(np.random.randint(1001))
        fun = obj_func_wrapper
        jac = jac_func_wrapper
        method = self.method
        sum_x = None
        sum_weight = 0
        t = 0
        while t < trials:
            t += 1
            x0 = self.init_func()
            self.n = x0.shape[0]
            self.lfunv = np.asarray([1e10] * self.n)
            self.delta = np.asarray([self.delta_init] * self.n)
            self.delta_x, self.ldelta_x = np.zeros(self.n), np.zeros(self.n)
            self.jacv, self.ljacv = np.zeros(self.n), np.zeros(self.n)
            print("\n------------------------------------------------------\n")
            print(method, "optimization", t)
            self.iters = 0
            from scipy.optimize import minimize
            cnts = [{'type': 'ineq', 'fun': lambda x:  x[d] - 1e-4} for d in xrange(1, self.n)]
            cnts.append({'type': 'ineq', 'fun': lambda x:  x[1] - x[0]})
            cnts.append({'type': 'ineq', 'fun': lambda x:  x[0] - 1e-1})
            try:
                minimize(fun=fun, x0=x0, tol=1e-10, method="COBYLA",
                         constraints=cnts, options={"maxiter":80*self.n, "disp":False})
            except Exception as e:
                print("Error !")
                trials += 1
                print(e)
                continue
            weight = abs(np.log2(self.iters)/(self.min_obj))
            sum_weight += weight
            if(sum_x is None):
                sum_x = self.best_x * weight
            else:
                sum_x += self.best_x * weight
            print("\nAfter", self.iters,\
                "iterations, best of this Optimization =", self.min_obj)
        x = sum_x/sum_weight
        print(x)
        print("\n------------------------------------------------------\n")
        return x

    def arbcd(self, fun, jac, x0):
        """
            Accelerated Randomized Block Coordinate Descent
            Max Improved for GPR:
            + back propagation (g, x and v)
        """

        n = x0.shape[0]
        mod = min(int(np.sqrt(n)) + 2, n-1)
        funv, lfunv = fun(x0), 1e20
        x, v, lx = x0, x0, x0
        g, lg = 0., 0.
        alpha, beta = 0., 0.
        bad, incre = 0, 0
        best_x, local_min_obj = x, 1e20
        from sklearn.cross_validation import KFold
        try:
            while(self.iters < max(300, 50*n)):
                if(self.iters % max(30, 3*n) == 1):
                    kf = KFold(n-1, mod, shuffle=True,
                               random_state=self.rand.random_integers(n))
                    ind_gps = [test_index+1 for train_index, test_index in kf]
                    self.delta = np.asarray([self.delta_init] * self.n)
                lg = g
                g = np.max(np.real(np.poly1d([1.,
                                   (mod*(g**2)-1)/n, -(g**2)]).r))
                alpha = (n-g*mod)/(g*((n**2)-mod))
                beta = (g*mod)/n
                y = v * alpha + (1-alpha) * x
                ind = ind_gps[self.rand.random_integers(len(ind_gps))-1]
                md = []
                for j in ind:
                    d = jac(y, j, False)
                    y[j] += d
                    y[j] = abs(y[j])
                    md.append(d)
                v = (1-beta)*v + beta*y + g*np.mean(md)
                lfunv = funv
                funv = fun(y)
                if(funv > lfunv):
                    incre += 1
                    if(incre > max(n, 20) and self.iters > 4*n):
                        break
                    g = lg
                    v = lx
                else:
                    incre = 0
                    lx = x
                    x = y
                if(funv < local_min_obj):
                    bad = 0
                    best_x = y
                    local_min_obj = funv
                    self.min_obj = local_min_obj
                    bg = g
                else:
                    bad += 1
                    if(abs(funv - local_min_obj) > 0.5*abs(local_min_obj)):
                        g = bg
                    elif(abs(funv - local_min_obj) < 1e-4*abs(local_min_obj)):
                        bad += 3
                    if(funv > local_min_obj*1.5):
                        bad += 1
                    if(bad > max(n*4, 50) and self.iters > 6*n):
                        break
                if(np.isinf(x).any() or np.isnan(x).any()):
                    x = best_x
            return best_x, local_min_obj
        except Exception as e:
            if(self.iters > 4*n):
                return best_x, local_min_obj
            print("\n"+str(e))
            return best_x, -1

    def arbcda(self, fun, jac, x0):
        """
            Accelerated Randomized Block Coordinate Descent
            Max Improved for GPR:
            + back propagation (g, x and v)
        """

        n = x0.shape[0]
        mod = min(int(np.sqrt(n)) + 2, n-1)
        funv, lfunv = fun(x0), 1e20
        x, v, lx = x0, x0, x0
        g, lg = 0., 0.
        alpha, beta = 0., 0.
        bad, incre = 0, 0
        best_x, local_min_obj = x, 1e20
        from sklearn.cross_validation import KFold
        try:
            while(self.iters < max(300, 50*n)):
                if(self.iters % max(30, 3*n) == 1):
                    kf = KFold(n-1, mod, shuffle=True,
                               random_state=self.rand.random_integers(n))
                    ind_gps = [test_index+1 for train_index, test_index in kf]
                    self.delta = np.asarray([self.delta_init] * self.n)
                lg = g
                g = np.max(np.real(np.poly1d([1.,
                                   (mod*(g**2)-1)/n, -(g**2)]).r))
                alpha = (n-g*mod)/(g*((n**2)-mod))
                beta = (g*mod)/n
                y = v * alpha + (1-alpha) * x
                ind = ind_gps[self.rand.random_integers(len(ind_gps))-1]
                md = []
                for j in ind:
                    d = jac(y, j, True)
                    y[j] += d
                    y[j] = abs(y[j])
                    md.append(d)
                v = (1-beta)*v + beta*y + g*np.mean(md)
                lfunv = funv
                funv = fun(y)
                if(funv > lfunv):
                    incre += 1
                    if(incre > max(n, 20) and self.iters > 4*n):
                        break
                    g = lg
                    v = lx
                else:
                    incre = 0
                    lx = x
                    x = y
                if(funv < local_min_obj):
                    bad = 0
                    best_x = y
                    local_min_obj = funv
                    self.min_obj = local_min_obj
                    bg = g
                else:
                    bad += 1
                    if(abs(funv - local_min_obj) > 0.5*abs(local_min_obj)):
                        g = bg
                    elif(abs(funv - local_min_obj) < 1e-4*abs(local_min_obj)):
                        bad += 3
                    if(funv > local_min_obj*1.5):
                        bad += 1
                    if(bad > max(n*4, 50) and self.iters > 6*n):
                        break
                if(np.isinf(x).any() or np.isnan(x).any()):
                    x = best_x
            return best_x, local_min_obj
        except Exception as e:
            if(self.iters > 4*n):
                return best_x, local_min_obj
            print("\n"+str(e))
            return best_x, -1
